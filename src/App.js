import React, { Component } from 'react';
import './App.css';
import Header from "./components/Header/Header";
import {Switch, Route} from "react-router-dom";
import Todo from "./containers/Todo/Todo";
import ToWatch from "./containers/ToWatch/ToWatch";
import Notes from "./containers/Notes/Notes";

import axios from 'axios';
axios.defaults.baseURL = 'https://kurlov-hw64-64d41.firebaseio.com/';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="App-body">
          <Switch>
            <Route path="/" exact component={Todo}/>
            <Route path="/todo" exact component={Todo}/>
            <Route path="/towatch" exact component={ToWatch}/>
            <Route path="/notes" exact component={Notes}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
