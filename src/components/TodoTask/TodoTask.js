import React from 'react';
import './TodoTask.css';

const TodoTask = ({task, deleteTask}) => {
  return (
    <div className="TodoTask">
      {task}
      <button className="TodoTask-deleteBtn" onClick={deleteTask}>Delete</button>
    </div>
  );
};

export default TodoTask;
