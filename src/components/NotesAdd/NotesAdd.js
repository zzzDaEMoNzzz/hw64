import React from 'react';
import './NotesAdd.css';

const NotesAdd = ({textAreaValue, onChange, onSubmit}) => {
  return (
    <form className="NotesAdd" onSubmit={onSubmit}>
      <textarea
        rows="5"
        placeholder="Add new note"
        value={textAreaValue}
        onChange={onChange}
      />
      <button>Add</button>
    </form>
  );
};

export default NotesAdd;
