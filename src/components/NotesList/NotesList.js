import React, {Fragment} from 'react';
import Note from "../Note/Note";

const NotesList = ({notes, saveNote, deleteNote}) => {
  const notesList = notes.map(note => (
    <Note
      key={note.id}
      id={note.id}
      value={note.note}
      saveNote={saveNote}
      deleteNote={deleteNote}
    />
  ));

  return (
    <Fragment>
      {notesList}
    </Fragment>
  );
};

export default NotesList;
